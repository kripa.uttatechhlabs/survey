import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { LoginService } from '../services/login.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  model = new User();
  constructor(
    private _authenticate: LoginService,
    private _route: Router,
   

  ) { }

  ngOnInit(): void {
    
  }

  authenticateUser(){
   
    this._authenticate.userLogin(this.model.email, this.model.password).subscribe(
      data => {
        if(data.status){
         // Swal.fire('Success', data.message, 'success');
          localStorage.setItem('auth_token', data.token);
          localStorage.setItem('token_type', data.token_type);
          this._route.navigate(['home/dashboard']);
        }
        else{
         // Swal.fire('Error', data.error || 'Something went wrong.', 'error');
        }
      }
    )
  }

}

export class User{
  email!: string;
  password!: string;
}
