import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from "@angular/common/http";
import { environment } from "src/environments/environment";

@Injectable({
  providedIn: 'root'
})
export class LoginService {

  constructor(private http : HttpClient) { }

  get baseRoute() {
    return environment.baseUrl;
  }

  userLogin(email: string, password: string): Observable<any> {
    const payload = {
      email: email,
      password: password,
    }
    return this.http.post(`${this.baseRoute}auth/login`, payload);
  }

  loggedIn(){
    return !!localStorage.getItem('auth_token');
  }

  loggedOut(): Observable<any>{
    return this.http.post(`${this.baseRoute}auth/logout`, {});
  }

}
